<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary' => $_ENV['WKHTMLPDF'],
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => $_ENV['WKHTMLIMG'],
        'options' => array(),
    ),


);
