<?php

class DeviceController extends \BaseController {

    private static $pages = 10;

    public function getIndex() {
        if (Auth::user()->isNormal()) {
            $devices =  Device::where('id_user', '=', Auth::user()->id)->paginate(self::$pages);
        } else {
            $devices = Device::join('users', 'users.id', '=', 'devices.id_user')
                ->orderBy('users.name')
                ->select('devices.*')
                ->with('status')
                ->with('user')
                ->paginate(self::$pages);
        }
        return View::make('device.index')->with(['devices' => $devices]);
    }

    public function getPartials($id = NULL) {
        $devices = Device::join('users', 'users.id', '=', 'devices.id_user')
                ->orderBy('users.name')
                ->select('devices.*')
                ->where('no_control', '=', NULL)
                ->with('status')
                ->with('user');
        if (isset($id)) {
            $devices = $devices->where('id_user', '=', $id);
        }
        return View::make('device.index')->with(['devices' => $devices->paginate(self::$pages)]);
    }

    public function getId($id) {
        $device = Device::find($id);
        if ($device) {
            return Response::make('Device View', 200);
        } else {
            return App::abort(404);
        }
    }

    public function getControl($control) {
        $device = Device::where('no_control', '=', $control);
        if ($device) {
            return Response::make('Device View', 200);
        } else {
            return App::abort(404);
        }
    }

    public function getCreate() {
        $users = [];
        $statuses = [];
        foreach (User::select('id', 'name', 't_user', 's_father')->get() as $u) {
            if ($u->isNormal()) {  // This might come back someday.
                $users[$u->id] = $u->name . ' ' . $u->s_father;
            }
        }
        foreach (Status::select('id', 'description')->where('description', '=', 'asignado')->get() as $d) {
            $statuses[$d['id']] = $d['description'];
        }
        return View::make('device.create')
            ->with([
            'users' => $users,
            'statuses' => $statuses,
        ]);
    }

    public function postCreate() {
        $device = Device::create(array_merge(Input::all(), ['id_status' => Status::where('description', '=', 'asignado')->first()->id]));
        if ($device->exists) {
            Session::flash('message', 'Device created succesfully');
            return Redirect::route('deviceIndex');
        } else {
            return Redirect::route('deviceCreateGet')
                    ->withErrors($device->errors())
                    ->withInput(Input::all());
        }
    }

    public function getUpdate($id) {
        $device = Device::find($id);
        if ($device) {
            $users = [];
            $statuses = [];
            foreach (User::select('id', 'name')->get()->toArray() as $d) {
                $users[$d['id']] = $d['name'];
            }
            foreach (Status::select('id', 'description')->get() as $s) {
                if (Auth::user()->isNormal() && ($s->isNotAssigned())) {
                    continue;
                }
                $statuses[$s->id] = $s->description;
            }
            return View::make('device.edit')
                        ->with([
                'device' => $device,
                'users' => $users,
                'statuses' => $statuses,
            ]);
        } else {
            return Redirect::route('deviceIndex')->withErrors(['Device' => 'Doesn\'t exist']);
        }

    }

    public function postUpdate($id) {
        $device = Device::find($id);

        $except = [];

        if (Auth::user()->isNormal()) {
            if ($device->id_user != Auth::user()->id) {
                App::abort(403, 'You don\t have permissions for this device');
            }
            $except = ['device_file', 'no_control', 'id_user'];
        } elseif (Auth::user()->isInvControl()) {
            $except = ['device_file', 'no_control', 'id_user', 'status'];
        } else {
            $except = ['device_file'];
        }

        if ($device) {
            $params = Input::except($except);
            if ($device->id_user != Input::get('id_user')) {
              DB::table('logs')->insert(
              array('id_status' => $device->id_status,
                    'id_user' => $device->id_user ,
                    'id_device' => $device->id,
                    'date' => date("Y-m-d"))
                   );
                
            }

            // Only the inventory control can upload a device_file
            $deviceFile = Input::hasFile('device_file') & Auth::user()->isInvControl() ? Input::file('device_file') : NULL;
            $device->fill($params);

            if (!is_null($device->no_control) && trim($device->no_control) == "") {
                $device->no_control = NULL;
            }

            $fileCorrect = TRUE;
            if ($deviceFile) {
                $fileCorrect = FALSE;
                $validator = Validator::make(['device_file' => $deviceFile], ['device_file' => 'mimes:jpeg,png,pdf']);
                if ($validator->passes()) {
                    if ($deviceFile->isValid()) {
                        $fileCorrect = TRUE;
                    } else {
                        $device->errors()->add('device_file', 'File is invalid');
                    }
                } else {
                    $device->errors()->merge($validator->messages());
                }    
            }
            
            if ($device->updateUniques()) {
                if ($deviceFile && $fileCorrect) {
                    // If file is correct and all the previous conditions have been met, save to disk.
                    $newPath = '/files/';
                    $newName = $device->no_control . '.' . $deviceFile->getClientOriginalExtension();
                    $deviceFile->move(public_path() . $newPath, $newName);
                    $device->device_file = $newPath . $newName;
                    $device->updateUniques();
                }
                Session::flash('message', 'Device succesfully updated' . $device->device_file);
                return Redirect::route('deviceIndex');
            }
            return Redirect::route('deviceUpdateGet', ['id' => $id])
                        ->withErrors($device->errors())
                        ->withInput(Input::except('device_file'));
        } else {
            return App::abort(404);
        }
    }

    public function postDelete($id) {
        $device = Device::find($id);
        if ($device) {
            $device->delete();
            Session::flash('message', 'Device succesfully destroyed');
            return Redirect::route('deviceIndex');
        } else {
            return Redirect::route('deviceIndex');
        }
    }

    public function getSearch($field, $value) {
        $device = Device::where($field, '=', $value)->first();
        if ($device) {
            $users = [];
            $statuses = [];
            foreach (User::select('id', 'name')->get()->toArray() as $d) {
                $users[$d['id']] = $d['name'];
            }
            foreach (Status::select('id', 'description')->get() as $s) {
                if (Auth::user()->isNormal() && ($s->isNotAssigned())) {
                    continue;
                }
                $statuses[$s->id] = $s->description;
            }
            Session::flash('message', 'Device found');
            return View::make('device.edit')
                        ->with([
                'device' => $device,
                'users' => $users,
                'statuses' => $statuses,
            ]);
        } else {
            return Redirect::back()->withErrors([$field => 'Dispositivo con '.$field.' = '.$value.' no existe']);
        }
    }

}
