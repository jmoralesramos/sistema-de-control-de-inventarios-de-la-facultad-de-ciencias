<?php

class DevicesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
    return View::make('devices.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
  public function nControl($dato)
	{
		$device = Device::where('no_control','=',$dato)->first();
		return View::make('devices.show')->with('device',$device);
	}
  public function nSerie($dato)
	{
		$device = Device::where('no_serial','=',$dato)->first();
		return View::make('devices.show')->with('device',$device);
	}
  public function ordenCompra($dato)
	{
		$device = Device::where('buy_order','=',$dato)->first();
		return View::make('devices.show')->with('device',$device);
	}
 	public function nPrograma($dato)
	{
		$device = Device::where('no_programa','=',$dato)->first();
		return View::make('devices.show')->with('device',$device);
	}
  public function verDevice($dato)
	{
		$device = Device::where('id','=',$dato)->first();
		return View::make('devices.verDevices')->with('device',$device);
	}
  public function verLogs($dato)
	{
    $logs="";
    $description="";
    //array_push($pila, "manzana", "arándano");
    foreach (DB::table('logs')->orderBy('date', 'desc')->where('id_device','=',$dato)->get()  as $d) {
          $id=$d->id;
          $logs[$id]["status"] = DB::table('statuses')->where('id', $d->id_status)->pluck('description');
          $logs[$id]["user"]   = DB::table('users')->where('id', $d->id_user)->pluck('name');
          $logs[$id]["device"] = DB::table('devices')->where('id', $d->id_device)->pluck('no_control');
          $logs[$id]["date"]   = $d->date   ;
          $description = DB::table('devices')->where('id', $d->id_device)->pluck('description'); 
        }
        
		return View::make('devices.verlogs')->with(['logs'=>$logs,'description'=>$description]);
	}


}
