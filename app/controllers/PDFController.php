<?php

// Include snappy

class PDFController extends BaseController {
     public function generatePDF() {
        $html = Input::get('html');
        return PDF::loadHtml($html)->download('report.html');
     }

     public function generateCarreerPDF($carreer) {
        $users = null;
        
        if ($carreer == 'admin'){
            $users = User::whereNull('id_carreer')->get();
            $fileName = 'devices_admins.pdf';
        } else if ($carreer == 0) {
            $users = User::all();
            $fileName = 'devices_all_carreers.pdf';
        } 
        else {
            $users = User::where('id_carreer', '=', $carreer)->get();
            $c = Carreer::find($carreer);
            $fileName = "devices_" . $c->name . '.pdf';
        }

        if (is_null($users)){
            return App::abort(404);
        } else {
            return PDF::loadView('reports.userCarreerTable', ['users' => $users])->download($fileName);
        } 
     }

     public function generateDevicesPDF($id) {
        $user = User::find($id);
        if (is_null($user)) {
            return App::abort(404);
        } else {
            return PDF::loadView('reports.userDevices', ['user' => $user])->download('user_device.pdf');
        }
    }

}
