<?php
class ReportsController extends BaseController {
    public function getPartials(){        
        
       
        if(Auth::user()->tUser->id == 1 || Auth::user()->tUser->id == 3){
            $devices = Device::where('no_control','=',null)->orderBy('d_entry','DESC')->get();        
            $devices->each(function ($device){
                $user = User::find($device->id_user);
                $carreer = is_null($user->id_carreer) ? 'Ninguna' : Carreer::find($user->id_carreer)->name;
                $device->t_name = $user->name . " ". $user->s_father. " ".$user->s_mother;
                $device->c_name = $carreer;
            });            
        }
        else{
             $id = Auth::user()->id;
             $devices = Device::where('no_control','=',null)->where('id_user','=',$id)->orderBy('d_entry','DESC')->get();    
              $devices->each(function ($device){              
                $device->t_name = Auth::user()->name . " ". Auth::user()->s_father. " ".Auth::user()->s_mother;
                $device->c_name = Auth::user()->carreer->name;
            });
        }
        return View::make('reports.partials', array('devices'=>$devices));
    }    
    
    public function getEP(){      
        $users = User::where('t_user','=',2)->orWhere('t_user','=',1)->orderBy('name','ASC')->get();
        $users->each(function($user){
            $user->devices = $user->devices;
        });
        $carreers = Carreer::all();
        $data = array();
        $data[] = $carreers;
        $data[] = $users;
        return View::make('reports.eprogram', array('data'=>$data));
    }  
    
    public function generatePartialsExcel(){        
         $styleArray = array(
	'font' => array(
		'bold' => true,
                                'size' => 12,
                                'color'=> array(
			'argb' => 'FFFFFFFF',
		),
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation' => 90,
		'startcolor' => array(
			'argb' => 'FFB2FF99',
		),
		'endcolor' => array(
			'argb' => 'FF71A900',
		),
	),
        );
       
        if(Auth::user()->isNormal()){
            $noEmployee = Auth::user()->no_employee;
            $filename = "ReporteParciales$noEmployee.xlsx";
            $id = Auth::user()->id;
            $devices = Device::where('no_control','=',null)->where('id_user','=',$id)->orderBy('d_entry','DESC')->get();    
            $devices->each(function ($device){              
                $device->t_name = Auth::user()->name . " ". Auth::user()->s_father. " ".Auth::user()->s_mother;
                $device->c_name = Auth::user()->carreer->name;
            });
        }
        else{
            $filename = "ReporteParciales.xlsx";
            $devices = Device::where('no_control','=',null)->orderBy('d_entry','DESC')->get();        
            $devices->each(function ($device){
                $user = User::find($device->id_user);
                $carreer = is_null($user->id_carreer) ? 'Ninguna' : Carreer::find($user->id_carreer)->name;
                $device->t_name = $user->name . " ". $user->s_father. " ".$user->s_mother."   (Num. Empleado: ".$user->no_employee.")";
                $device->c_name = $carreer;
            }); 
        }        
        $filepath = public_path()."/excel/".$filename;
        $objPHPExcel = new PHPExcel();
        $userName = Auth::user()->name." ".Auth::user()->s_father." ".Auth::user()->s_mother;
        $objPHPExcel->getProperties()->setCreator($userName);
        $objPHPExcel->getProperties()->setLastModifiedBy($userName);
        $objPHPExcel->setActiveSheetIndex(0);
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fecha = date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
            $header = "Reporte de Equipos Parciales\nFacultad de Ciencias\n$fecha";
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $header); 
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(48);                  
            $objPHPExcel->getActiveSheet()->mergeCells("A1:G1");
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Fecha');
            $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Nombre del maestro');
            $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Descripción');
            $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Proveedor');
            $objPHPExcel->getActiveSheet()->setCellValue('E2', 'No. Programa');
            $objPHPExcel->getActiveSheet()->setCellValue('F2', 'No. Serie');
            $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Carrera');          
            $ind = 3;
            if(count($devices) > 0){
                $devices->each(function ($device) use (&$ind, $objPHPExcel){                    
                   $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, strval($device->d_entry));
                   $objPHPExcel->getActiveSheet()->setCellValue('B'.$ind, strval($device->t_name));
                   $objPHPExcel->getActiveSheet()->setCellValue('C'.$ind, strval($device->description));
                   $objPHPExcel->getActiveSheet()->setCellValue('D'.$ind, strval($device->provider));
                   $objPHPExcel->getActiveSheet()->getCell('E'.$ind)->setValueExplicit(strval($device->no_programa), PHPExcel_Cell_DataType::TYPE_STRING);
                   $objPHPExcel->getActiveSheet()->getCell('F'.$ind)->setValueExplicit(strval($device->no_serial), PHPExcel_Cell_DataType::TYPE_STRING);
                   $objPHPExcel->getActiveSheet()->setCellValue('G'.$ind, strval($device->c_name));
                   $ind += 1;
                });
                $objPHPExcel->getActiveSheet()->getStyle('A1:G2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
                $objPHPExcel->getActiveSheet()->getStyle('A1:G999')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objPHPExcel->getActiveSheet()->getStyle('A1:G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A3:G999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            }
                       
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filepath);
        $file = $filepath;
        $headers = array('Content-Type' => '  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response = Response::download($file,$filename,$headers);
        ob_end_clean();
        return $response;
    }

    public function generateGeneralExcel(){

         $styleArray = array(
	'font' => array(
		'bold' => true,
                                'size' => 12,
                                'color'=> array(
			'argb' => 'FFFFFFFF',
		),
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation' => 90,
		'startcolor' => array(
			'argb' => 'FFB2FF99',
		),
		'endcolor' => array(
			'argb' => 'FF71A900',
		),
	),
        );
          $listHStyle = array(
            'font' => array(
                    'bold' => true,
            ),
        );
        $user = null;
        $users = User::where('t_user','=',2)->orderBy('name','ASC')->get();       
        $carreers = Carreer::orderBy('name','ASC')->get();     
        if(is_null($users)){
            return App::abort(404);
        }
        else{
            $filename = "ReporteGeneral.xlsx";
            $filepath = public_path()."/excel/".$filename;
            $objPHPExcel = new PHPExcel();
            $userName = Auth::user()->name." ".Auth::user()->s_father." ".Auth::user()->s_mother;
            $objPHPExcel->getProperties()->setCreator($userName);
            $objPHPExcel->getProperties()->setLastModifiedBy($userName);
            $objPHPExcel->setActiveSheetIndex(0);                  
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fecha = date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
            $header = "Reporte General de Asignación de Equipo\nFacultad de Ciencias\n$fecha";
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $header); 
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(48);                  
            $objPHPExcel->getActiveSheet()->mergeCells("A1:G1");
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'No. Control');
            $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Descripción');
            $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Orden de Compra');
            $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Marca');
            $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Serie');
            $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Estado');
            $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Ubicación');   
            $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Observaciones'); 
            $ind = 3;       
            $carreers->each(function($carreer) use($users, &$ind, $objPHPExcel, $listHStyle){
                $c_id = $carreer->id;
                $users->each(function($user) use (&$ind, $objPHPExcel, $c_id, $listHStyle){
                    if($c_id == $user->carreer->id && count($user->devices) > 0)
                    {
                        $listHead =$user->carreer->name.": ". $user->name." ".$user->s_father." ".$user->s_mother."   (Num. Empleado: ".$user->no_employee.")";
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, $listHead); 
                        $objPHPExcel->getActiveSheet()->mergeCells("A$ind:H$ind");
                        $objPHPExcel->getActiveSheet()->getStyle("A$ind:H$ind")->applyFromArray($listHStyle);
                        $ind += 1;
                        $devices = $user->devices;
                        $devices->each(function($device) use(&$ind, $objPHPExcel){
                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, strval($device->no_control)); 
                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$ind, strval($device->description));                         
                            $objPHPExcel->getActiveSheet()->getCell('C'.$ind)->setValueExplicit(strval($device->buy_order), PHPExcel_Cell_DataType::TYPE_STRING);
                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$ind, strval($device->brand)); 
                            $objPHPExcel->getActiveSheet()->getCell('E'.$ind)->setValueExplicit(strval($device->no_serial), PHPExcel_Cell_DataType::TYPE_STRING);
                            $objPHPExcel->getActiveSheet()->setCellValue('F'.$ind, strval($device->status->description)); 
                            $objPHPExcel->getActiveSheet()->setCellValue('G'.$ind, strval($device->location)); 
                            $objPHPExcel->getActiveSheet()->setCellValue('H'.$ind, strval($device->observations)); 
                            $ind += 1;
                        });                   
                    }
                });
            });       
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('H3:H'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('A1:H999')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A3:H999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save($filepath);
            $file = $filepath;
            $headers = array('Content-Type' => '  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $response = Response::download($file,$filename,$headers);
            ob_end_clean();
            return $response;
        }
    }
        
    public function generateCarreerExcel($carreer){
        $styleArray = array(
	'font' => array(
		'bold' => true,
                                'size' => 12,
                                'color'=> array(
			'argb' => 'FFFFFFFF',
		),
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation' => 90,
		'startcolor' => array(
			'argb' => 'FFB2FF99',
		),
		'endcolor' => array(
			'argb' => 'FF71A900',
		),
	),
        );
         $listHStyle = array(
                'font' => array(
                        'bold' => true,
                ),
            );
         $users = null;
        if($carreer == 'admin'){
            $filename = "ReporteAministradores.xlsx";
            $users = User::where('t_user','=',1)->get();
        }
        else{
            $c_name = Carreer::find($carreer);            
            $c_nameNS = preg_replace('/\s+/', '', $c_name->name);;
            $filename = "Reporte".$c_nameNS.".xlsx";
            $users = User::where('id_carreer','=',$carreer)->get();
        }
        if (is_null($users)){
            return App::abort(404);
        }
        else{
            $filepath = public_path()."/excel/".$filename;
            $objPHPExcel = new PHPExcel();
            $userName = Auth::user()->name." ".Auth::user()->s_father." ".Auth::user()->s_mother;
            $objPHPExcel->getProperties()->setCreator($userName);
            $objPHPExcel->getProperties()->setLastModifiedBy($userName);
            $objPHPExcel->setActiveSheetIndex(0);           
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fecha = date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
            if($carreer != "admins"){
                $carreerN = $c_name->name;
                $header = "Reporte General de Asignación de Equipo\nFacultad de Ciencias\n$carreerN\n$fecha";
                $objPHPExcel->getActiveSheet()->setCellValue('A1', $header); 
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(65);       
            }
            else{
                $header = "Reporte General de Asignación de Equipo\nFacultad de Ciencias\n$fecha";
                $objPHPExcel->getActiveSheet()->setCellValue('A1', $header); 
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(48);       
            }                
            $objPHPExcel->getActiveSheet()->mergeCells("A1:G1");
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'No. Control');
            $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Descripción');
            $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Orden de Compra');
            $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Marca');
            $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Serie');
            $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Estado');
            $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Ubicación');   
            $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Observaciones');            
            $ind = 3;
            $users->each(function($user) use (&$ind, $objPHPExcel, $listHStyle){
                if(count($user->devices) > 0){
                    $listHead =$user->carreer->name.": ". $user->name." ".$user->s_father." ".$user->s_mother."   (Num. Empleado: ".$user->no_employee.")";
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, $listHead); 
                    $objPHPExcel->getActiveSheet()->mergeCells("A$ind:H$ind");
                    $objPHPExcel->getActiveSheet()->getStyle("A$ind:H$ind")->applyFromArray($listHStyle);
                    $ind += 1;
                    $devices = $user->devices;
                    $devices->each(function($device) use (&$ind, $objPHPExcel){
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, strval($device->no_control)); 
                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$ind, strval($device->description));                         
                        $objPHPExcel->getActiveSheet()->getCell('C'.$ind)->setValueExplicit(strval($device->buy_order), PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$ind, strval($device->brand)); 
                        $objPHPExcel->getActiveSheet()->getCell('E'.$ind)->setValueExplicit(strval($device->no_serial), PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$ind, strval($device->status->description)); 
                        $objPHPExcel->getActiveSheet()->setCellValue('G'.$ind, strval($device->location)); 
                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$ind, strval($device->observations)); 
                        $ind += 1;
                    });
                }
            });           
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('H3:H'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('A1:H999')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A3:H999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save($filepath);
            $file = $filepath;
            $headers = array('Content-Type' => '  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $response = Response::download($file,$filename,$headers);
            ob_end_clean();
            return $response;
        }
    }
    
    public function generateDevicesExcel($id){
          $styleArray = array(
	'font' => array(
		'bold' => true,
                                'size' => 12,
                                'color'=> array(
			'argb' => 'FFFFFFFF',
		),
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation' => 90,
		'startcolor' => array(
			'argb' => 'FFB2FF99',
		),
		'endcolor' => array(
			'argb' => 'FF71A900',
		),
	),
        );
         $listHStyle = array(
                'font' => array(
                        'bold' => true,
                ),
            );
         $user = null;
         if(Auth::user()->isNormal())
            $user = Auth::user();
        else
             $user = User::find($id);
        if (is_null($user)){
            return App::abort(404);
        }
        else{
            $filename = "ReporteUsuario".$user->no_employee.".xlsx";
            $filepath = public_path()."/excel/".$filename;
            $objPHPExcel = new PHPExcel();
            $userName = Auth::user()->name." ".Auth::user()->s_father." ".Auth::user()->s_mother;
            $objPHPExcel->getProperties()->setCreator($userName);
            $objPHPExcel->getProperties()->setLastModifiedBy($userName);
            $objPHPExcel->setActiveSheetIndex(0);           
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fecha = date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
            $header = "Reporte de Equipos Asignados\nFacultad de Ciencias\n$fecha";
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $header); 
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(48);                  
            $objPHPExcel->getActiveSheet()->mergeCells("A1:G1");
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'No. Control');
            $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Descripción');
            $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Orden de Compra');
            $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Marca');
            $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Serie');
            $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Estado');
            $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Ubicación');   
            $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Observaciones');            
            $ind = 3;
            $listHead =$user->carreer->name.": ". $user->name." ".$user->s_father." ".$user->s_mother."   (Num. Empleado: ".$user->no_employee.")";
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, $listHead); 
            $objPHPExcel->getActiveSheet()->mergeCells("A$ind:H$ind");
            $objPHPExcel->getActiveSheet()->getStyle("A$ind:H$ind")->applyFromArray($listHStyle);
            $ind += 1;
            $devices = $user->devices;
            $devices->each(function($device) use (&$ind, $objPHPExcel){
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$ind, strval($device->no_control)); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$ind, strval($device->description));                         
                $objPHPExcel->getActiveSheet()->getCell('C'.$ind)->setValueExplicit(strval($device->buy_order), PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$ind, strval($device->brand)); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$ind)->setValueExplicit(strval($device->no_serial), PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$ind, strval($device->status->description)); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$ind, strval($device->location)); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$ind, strval($device->observations)); 
                $ind += 1;
            });    
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(60);
            $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('H3:H'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
            $objPHPExcel->getActiveSheet()->getStyle('A1:H999')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A3:H999')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save($filepath);
            $file = $filepath;
            $headers = array('Content-Type' => '  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $response = Response::download($file,$filename,$headers);
            ob_end_clean();
            return $response;
        }
    }
    
}
?>