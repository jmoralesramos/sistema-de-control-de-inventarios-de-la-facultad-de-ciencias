<?php

class UserController extends BaseController {

    public function getIndex() {
        $users = User::paginate(10);
        return View::make('users/index')->with(['users' => $users]);
    }
    
    public function getInactives() {
        $users = User::where('t_user','=',2)->where('active','=',1)->where('status','=',0)->orderBy('name')->get(); 
        $tUsers = User::where('t_user','=',2)->get();
        return View::make('users/inactives')->with(['users' => $users, 'tUsers'=>$tUsers]);
    }
    
    public function getNotification() {
        $users = User::where('t_user','=',2)->where('active','=',1)->where('status','=',0)->orderBy('name')->get();                
        return View::make('users/sendmailnotification')->with(['users' => $users]);
    }
    
    public function postNotification() {
        $params = Input::all();  
        $signature =  Auth::user()->name.' '. Auth::user()->s_father.' '.Auth::user()->s_mother;        
        $emails = explode(",", $params['mails']); 
        $fails = array();
        for($i = 0, $tam = count($emails); $i < $tam; $i ++){
            $emails[$i] = str_replace(' ', '', $emails[$i]);
            $validator = Validator::make(
                 array(                                
                     'email' => $emails[$i]
                 ),
                 array(                                
                     'email' => 'required|email'
                 )
             );
            if ($validator->fails())
            {
                if(count($fails) == 0){
                    $fails[] = "Los siguientes correos no son validos:";
                    $fails[] =$emails[$i];
                }
                else
                 $fails[] =$emails[$i];
            }
        }
        if( count($fails) == 0){
            for($i = 0, $tam = count($emails); $i < $tam; $i ++){                
                Mail::send('users.mailBody',array('messages'=>$params['message'],'signature'=> $signature),function($message) use ($emails,$i,$params){ 
                    $message->to($emails[$i])->subject($params['subject']);                
                });                     

            }       
            return View::make('users/mailsent')->with(['totalMails' => count($emails)]);
        }
        else{
            return Redirect::route('userSendMail')->withErrors($fails)
                    ->withInput(Input::all());;
        }
    }
    
    public function getMasiveMail() {
        $users = User::where('t_user','=',2)->where('active','=',1)->orderBy('name')->get();                
        return View::make('users/sendmasivemail')->with(['users' => $users]);
    }
    
    public function postMasiveMail() {
        $params = Input::all();  
        $signature =  Auth::user()->name.' '. Auth::user()->s_father.' '.Auth::user()->s_mother;        
        $emails = explode(",", $params['mails']); 
        for($i = 0, $tam = count($emails); $i < $tam; $i ++){
            $emails[$i] = str_replace(' ', '', $emails[$i]);
            Mail::send('users.mailBody',array('messages'=>$params['message'],'signature'=> $signature),function($message) use ($emails,$i,$params){ 
                $message->to($emails[$i])->subject($params['subject']);                
            });                     
            
        }       
        User::where('t_user','=',2)->where('active','=',1)->update(array('status'=>0));
        return View::make('users/masivemailsent')->with(['totalMails' => count($emails)]);
    }
    

    public function getCreate() {
        $carreers = ['0' => 'Ninguna'];
        $userTypes = [];
        foreach(Carreer::select('id', 'name')->get() as $carreer) {
            $carreers[$carreer->id] = $carreer->name;
        }
        foreach(UserType::select('id', 'name')->get() as $userType) {
            $userTypes[$userType->id] = $userType->name;
        }
        return View::make('users.create')->with(['carreers' => $carreers, 'userTypes' => $userTypes]);
    }

    public function postCreate() {
        $params = Input::all();
        $params['password'] = Hash::make($params['password']);// TODO: Move this to the model also in update
        $params['status'] = Input::has('status') ? 1 : 0;
        $params['active'] = Input::has('active') ? 1 : 0;
        if (Input::get('id_carreer') == 0) {
            $params['id_carreer'] = NULL;
        } else {
            $params['id_carreer'] = Input::get('id_carreer');
        }
        $user = User::create($params);
        if ($user->exists) {
            return Redirect::route('userIndex')
            	->with('message', 'User succesfully created');
        } else {
            return Redirect::route('userCreateGet')
                ->withErrors($user->errors())
                ->withInput(Input::except('password'));
        }
    }

    public function getUpdate($id) {
        if (Auth::user()->isNormal() && Auth::user()->id != $id) {
            return App::abort(403);
        }
        $user = User::find($id);
        $carreers = ['0' => 'Ninguna'];
        $userTypes = [];
        foreach(Carreer::select('id', 'name')->get() as $carreer) {
            $carreers[$carreer->id] = $carreer->name;
        }
        foreach(UserType::select('id', 'name')->get() as $userType) {
            $userTypes[$userType->id] = $userType->name;
        }
        return View::make('users.edit')->with(['user' => $user, 'carreers' => $carreers, 'userTypes' => $userTypes]);
    }

    public function postUpdate($id) {
        $user = User::find($id);

        if ($user) {
            $params = Input::except(['password', 'id_carreer']);
            if (trim(Input::get('password')) !== '') {
                $params['password'] = Hash::make(Input::get('password'));
            }
            $params['status'] = Input::has('status') ? 1 : 0;
            $params['active'] = Input::has('active') ? 1 : 0;
            if (Input::get('id_carreer') == 0) {
                $params['id_carreer'] = NULL;
            } else {
                $params['id_carreer'] = Input::get('id_carreer');
            }
            //return $params;
            $user->fill($params);
            if ($user->updateUniques(['password' => 'max:100'])) {
                Session::flash('message', 'User succesfully updated');
                return Redirect::back();
            } else {
                return Redirect::route('userUpdateGet', ['id' => $id])
                    ->withErrors($user->errors())
                    ->withInput(Input::except('password'));
            }
        } else {
            return App::abort(404);
        }

    }

    public function postDelete($id) {
        $user = User::find($id);
        if($user) {
            $user->delete();
            Session::flash('message', 'Succesfully erased user.');
            return Redirect::route('userIndex');
        } else {
            return App::abort(404);
        }
    }

}
