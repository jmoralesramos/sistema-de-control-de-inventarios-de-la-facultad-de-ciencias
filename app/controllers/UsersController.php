<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('hello');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function searchRequest()
	{
    $users = User::all();
		return View::make('users.searchResponse ')->with('users',$users);
	}
	public function searchResponse($dato)
	{
    $users = User::where('name','LIKE',$dato."%")
                      ->orwhere('s_father','LIKE',$dato."%")
                      ->orwhere('s_mother','LIKE',$dato."%")
                      ->get();
		return View::make('users.searchResponse')->with('users',$users);
	}
	public function myDevices($dato)
	{
		$user = User::find($dato);
		return View::make('devices.myDevices')->with(['user' => $user]);
	}
}
