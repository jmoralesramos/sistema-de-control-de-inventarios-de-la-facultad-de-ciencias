<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesFacultiesTables extends Migration
{
    
    /**
     * Create status and carrer tables
     *
     * @return void
     */
    public function up() {

    	try {
	        Schema::create('faculties', function ($table) {
	            $table->increments('id');
	            $table->string('name', 100);
	        });
    	} catch (Exception $e) {
    		Schema::drop('faculties');
    		throw $e;
    	}

    	try {
	        Schema::create('statuses', function ($table) {
	            $table->increments('id');
	            $table->string('description', 30);
	        });	
    	} catch (Exception $e) {
    		Schema::drop('statuses');
    		throw $e;
    	}
    }
    
    /**
     * Remove the status and carrer tables
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('faculties');
        Schema::dropIfExists('statuses');
    }
}
