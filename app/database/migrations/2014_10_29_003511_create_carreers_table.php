<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		try {
			Schema::create('carreers', function($table) {
				$table->increments('id');
				$table->integer('id_faculty')->unsigned();
				$table->string('name', 100);
				$table->foreign('id_faculty')->references('id')->on('faculties');
			});			
		} catch (Exception $e) {
			Schema::drop('carreers');
			throw $e;
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('carreers');
	}

}
