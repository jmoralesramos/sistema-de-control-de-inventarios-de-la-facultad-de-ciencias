<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        try {
            Schema::create('users', function ($table) {
                $table->increments('id');
                $table->integer('id_carreer')->unsigned()->nullable();
                $table->string('name', 64);
                $table->string('s_father', 32);
                $table->string('s_mother', 32);
                $table->integer('no_employee')->unique();
                $table->string('email', 255)->unique();
                $table->string('t_user', 10);
                $table->string('password', 100);
                $table->boolean('status');
                $table->boolean('active');

                $table->foreign('id_carreer')->references('id')->on('carreers');
            });
        }
        catch(Exception $e) {
        	Schema::dropIfExists('users');
        	throw $e;
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
        Schema::dropIfExists('users');
        
    }
}
