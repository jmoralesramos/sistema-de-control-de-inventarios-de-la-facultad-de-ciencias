<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		try {
            Schema::create('devices', function ($table) {
                $table->increments('id');
                $table->integer('id_user')->unsigned();
				$table->integer('id_status')->unsigned();
				$table->string('no_control', 8)->unique()->nullable();
				$table->text('description');		
				$table->string('buy_order', 12);
				$table->string('brand', 30);
				$table->string('no_serial', 20);
				$table->integer('no_programa');		
				$table->text('location');		
				$table->text('observations');		
				$table->date('d_entry');		
				$table->string('provider', 50);

				$table->foreign('id_user')->references('id')->on('users');
				$table->foreign('id_status')->references('id')->on('statuses');
            });
        }
        catch(Exception $e) {
        	Schema::dropIfExists('devices');
        	throw $e;
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('devices');
	}

}
