<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        try {
            Schema::create('logs', function ($table) {
                $table->increments('id');
                $table->integer('id_status')->unsigned();
                $table->integer('id_user')->unsigned();
                $table->integer('id_device')->unsigned();
                $table->date('date');

				$table->foreign('id_user')->references('id')->on('users');
				$table->foreign('id_status')->references('id')->on('statuses');
				$table->foreign('id_device')->references('id')->on('devices');
            });
        } catch(Exception $e) {
            Schema::dropIfExists('logs');
            throw $e;
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('logs');
    }
}
