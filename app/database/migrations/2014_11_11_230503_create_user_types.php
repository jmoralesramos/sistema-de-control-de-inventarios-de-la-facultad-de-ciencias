<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserTypes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        try {
            Schema::create('user_types', function ($table) {
                $table->increments('id');
                $table->string('name', 16)->unique();
            });
        }
         catch (Exception $e) {
            Schema::dropIfExists('user_types');
            throw $e;
        }

        Schema::table('users', function ($table) {
            $table->dropColumn('t_user');
        });

        Schema::table('users', function ($table) {
            $table->integer('t_user')->unsigned();
            $table->foreign('t_user')->references('id')->on('user_types');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('users', 't_user')) {
            Schema::table('users', function ($table) {
                $table->dropForeign('users_t_user_foreign');
                $table->dropColumn('t_user');
            });
        }
        Schema::table('users', function ($table) {
            $table->string('t_user', 10);
        });
        Schema::dropIfExists('user_types');
    }

}
