<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDeviceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		try {
			Schema::table('devices', function($table) {
				$table->text('device_file')->nullable();
			});
		} catch (Exception $e){
			throw $e;
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('devices', function($table) {
			if(Schema::hasColumn('devices', 'device_file')) {
				$table->dropColumn('device_file');
			}
		});
	}

}
