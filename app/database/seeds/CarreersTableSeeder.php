<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CarreersTableSeeder extends Seeder {

    public function run()
    {

        if (Carreer::all()->count()) {
                echo "This seed has already been run\n";
                return false;
        }

        Carreer::create([
            'name' => 'Licenciatura en Ciencias Computacionales',
            'id_faculty' => Faculty::where('name', '=', 'Facultad de Ciencias')->first()->id
        ]);

        Carreer::create([
            'name' => 'Licenciatura en Matemáticas',
            'id_faculty' => Faculty::where('name', '=', 'Facultad de Ciencias')->first()->id
        ]);

        Carreer::create([
            'name' => 'Licenciatura en Física',
            'id_faculty' => Faculty::where('name', '=', 'Facultad de Ciencias')->first()->id
        ]);

        Carreer::create([
            'name' => 'Licenciatura en Biología',
            'id_faculty' => Faculty::where('name', '=', 'Facultad de Ciencias')->first()->id
        ]);

    }

}
