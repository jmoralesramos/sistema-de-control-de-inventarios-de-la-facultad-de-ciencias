<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('StatusesTableSeeder');
        $this->call('FacultiesTableSeeder');
        $this->call('CarreersTableSeeder');
        $this->call('UserTypesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('DevicesTableSeeder');
        // $this->call('DeviceLogsTableSeeder');
    }

}
