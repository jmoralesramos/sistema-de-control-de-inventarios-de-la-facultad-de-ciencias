<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DeviceLogsTableSeeder extends Seeder {

	public function run()
	{

		if (DeviceLog::all()->count()) {
			echo "This seed has already been run";
			return false;
		}

		$faker = Faker::create();

		$statuses = Status::all()->toArray();
		$users = User::all()->toArray();
		$devices = Device::all()->toArray();

		foreach(range(1, 10) as $index)
		{
			DeviceLog::create([
				'id_status' => $faker->randomElement($statuses)['id'],
				'id_user' => $faker->randomElement($users)['id'],
				'id_device' => $faker->randomElement($devices)['id'],
				'date' => $faker->date
			]);
		}
	}

}