<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DevicesTableSeeder extends Seeder {

	public function run()
	{

		// if (Device::all()->count()) {
		// 	echo "This seed has already been run\n";
		// 	return false;
		// }

		$faker = Faker::create();

		$users = User::all()->toArray();
		$statuses = Status::all()->toArray();

		foreach(range(1, 300) as $index)
		{
			$r = ((float) rand()/(float)getrandmax());
			Device::create([
				'id_user' =>$faker->randomElement($users)['id'],
				'id_status' =>$faker->randomElement($statuses)['id'],
				'no_control' => $r < .5 ? $faker->numerify('########') : NULL,
				'description' =>$faker->text(100),
				'buy_order' =>$faker->numerify('############'),
				'brand' =>$faker->text(25),
				'no_serial' =>$faker->numerify('####################'),
				'no_programa' =>$faker->numerify('#########'),
				'location' =>$faker->city,
				'observations' =>$faker->text(100),
				'd_entry' =>$faker->date,
				'provider' =>$faker->company,
			]);
		}
	}

}