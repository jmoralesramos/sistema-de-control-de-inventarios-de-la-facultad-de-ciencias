<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FacultiesTableSeeder extends Seeder {

	public function run()
	{

		if (Faculty::all()->count()) {
			echo "This seed has already been run\n";
			return false;
		}

		Faculty::create([
			'name' => 'Facultad de Ciencias'
		]);
	}

}