<?php

class StatusesTableSeeder extends Seeder {

	public function run()
	{

		if (Status::all()->count()) {
			echo "This seed has already been run\n";
			return false;
		}

		$statusNames = array('no asignado', 'asignado', 'fuera de servicio', 'prestado');

		foreach($statusNames as $name)
		{
			$status = new Status;
			$status->description = $name;
			$status->save();
		}
	}

}