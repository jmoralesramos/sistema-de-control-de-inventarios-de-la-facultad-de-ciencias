<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTypesTableSeeder extends Seeder {

	public function run()
	{

		if (UserType::all()->count()) {
			echo "This seed has already been run\n";
			return false;
		}
		
		UserType::create([
			'name' => 'admin',
		]);
        UserType::create([
            'name' => 'nonadmin',
        ]);
        UserType::create([
            'name' => 'inv_control',
        ]);
	}

}