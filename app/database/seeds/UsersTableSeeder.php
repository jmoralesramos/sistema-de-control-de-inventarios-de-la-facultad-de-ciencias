<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

    public function run() {

        if (User::all()->count()) {
            echo "This seed has already been run\n";
            return false;
        }

        $faker = Faker::create('es_ES');
        $userTypes = UserType::all()->toArray();

        $max = Carreer::all()->count();

        $admin = User::create([
            'id_carreer' => null,
            'name' => 'Admin',
            's_father' => 'Lopez',
            's_mother' => 'Garcia',
            'no_employee' => '000000',
            'email' => 'admin@gmail.com',
            't_user' => UserType::where('name', '=', 'admin')->first()->id,
            'password' => Hash::make('admin'),
            'status' => 1,
            'active' => 1,
        ]);

        $inv = User::create([
            'id_carreer' => null,
            'name' => 'Inventory',
            's_father' => 'Lopez',
            's_mother' => 'Garcia',
            'no_employee' => '000001',
            'email' => 'inventory@gmail.com',
            't_user' => UserType::where('name', '=', 'inv_control')->first()->id,
            'password' => Hash::make('inventory'),
            'status' => 1,
            'active' => 1,
        ]);

        $me = User::create([
            'id_carreer' => 1,
            'name' => 'Simple',
            's_father' => 'User',
            's_mother' => 'Useringen',
            'no_employee' => '000004',
            'email' => 'user@gmail.com',
            't_user' => UserType::where('name', '=', 'nonadmin')->first()->id,
            'password' => Hash::make('secret'),
            'status' => 1,
            'active' => 1,
        ]);

        foreach (range(1, 10) as $index) {
            User::create([
                'id_carreer' => $faker->numberBetween(1, $max),
                'name' => $faker->name,
                's_father' => $faker->lastName,
                's_mother' => $faker->lastName,
                'no_employee' => $faker->unique()->numerify('######'),
                'email' => $faker->email,
                't_user' => 2,  // NonAnmin users
                'password' => Hash::make('secret'),
                'status' => mt_rand(0, 1),
                'active' => mt_rand(0, 1),
            ]);
        }
    }

}
