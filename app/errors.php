<?php
if (Config::get('app.debug') == false) {
    App::error(function(Exception $exception, $code){
        Log::error($exception);
        return View::make('errors.'.$code)->with(['exception' => $exception]);
    });
}
?>