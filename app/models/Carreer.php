<?php

class Carreer extends Eloquent {

	public $timestamps = false;
	
	public function faculty() {
		return $this->belongsTo('Faculty', 'id_faculty');
	}

	public function users() {
		return $this->hasMany('User', 'id_carreer');
	}

}