<?php

use LaravelBook\Ardent\Ardent;

class Device extends Ardent {

    public $timestamps = false;

    protected $guarded = ['id'];

    public static $rules = [
        'id_user' => 'required|exists:users,id',
        'id_status' => 'required|exists:statuses,id',
        'no_control' => 'max:10|unique:devices',
        'description' => 'required',
        'buy_order' => 'required|max:12',
        'brand' => 'required|max:30',
        'no_serial' => 'required|max:20',
        'no_programa' => 'required|numeric',
        'location' => '',
        'observations' => '',
        'd_entry' => 'required|date',
        'provider' => 'required|max:50',
    ];

    public static function boot() {
        parent::boot();

        Device::created(function($device) {

        });

        Device::updated(function($device){

        });

    }

    public function user() {
        return $this->belongsTo('User', 'id_user');
    }

    public function status() {
        return $this->belongsTo('Status', 'id_status');
    }

    public function log() {
        return $this->hasMany('DeviceLog', 'id_device');
    }

}