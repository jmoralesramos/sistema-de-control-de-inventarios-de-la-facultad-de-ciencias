<?php

use LaravelBook\Ardent\Ardent;

class DeviceLog extends Ardent {

	protected $table = 'logs';

	public static $rules = [
		'id_status' => 'required|exists:statuses,id',
		'id_user' => 'required|exists:users,id',
		'id_device' => 'required|exists:devices,id',
		'date' => 'required|date',
	];

	public $timestamps = false;

	public function status() {
		$this->belongsTo('Status', 'id_status');
	}

	public function user() {
		$this->belongsTo('User', 'id_user');
	}

	public function device() {
		$this->belongsTo('Device', 'id_device');
	}

}