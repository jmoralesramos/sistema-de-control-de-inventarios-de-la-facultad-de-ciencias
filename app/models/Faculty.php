<?php

class Faculty extends Eloquent {

	public $timestamps = false;
	
	public function carreers() {
		return $this->hasMany('Carreer', 'id_faculty');
	}

}