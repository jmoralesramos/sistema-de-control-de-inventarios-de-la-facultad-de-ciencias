<?php

class Status extends Eloquent {

	public $timestamps = false;
	
	public function devices() {
		return $this->hasMany('Device', 'id_status');
	}

	public function logs() {
		return $this->hasMany('DeviceLog', 'id_status');
	}

    public function isAssigned() {
        return $this->description == 'asignado';
    }

    public function isNotAssigned() {
        return $this->description == 'no asignado';
    }

    public function isOutOfService() {
        return $this->description == 'fuera de servicio';
    }

    public function isLent() {
        return $this->description == 'prestado';
    }

}