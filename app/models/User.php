<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;
use LaravelBook\Ardent\Ardent;

class User extends Ardent implements UserInterface, RemindableInterface {

    public static $rules = [
        'id_carreer' => 'exists:carreers,id',
        'name' => 'required|between:4,64',
        's_father' => 'required|between:4,32',
        's_mother' => 'required|between:4,32',
        'no_employee' => 'required|unique:users|numeric',
        'email' => 'required|email|unique:users',
        't_user' => 'required|exists:user_types,id', // This should be in a separate table I think. Marco Macareno
        'password' => 'required|max:100', // This numbers could be abstracted, but I don't think so.
        'status' => 'required|boolean',
        'active' => 'required|boolean',
    ];

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public $timestamps = false;

    protected $guarded = ['id'];

    public function carreer() {
        return $this->belongsTo('Carreer', 'id_carreer');
    }

    public function tUser() {
        return $this->belongsTo('UserType', 't_user');
    }

    public function devices() {
        return $this->hasMany('Device', 'id_user')->orderBy('d_entry','DESC');
    }

    public function logs() {
        return $this->hasMany('DeviceLog', 'id_user');
    }

    public function isAdmin() {
        return $this->tUser->name == 'admin';
    }

    public function isInvControl() {
        return $this->tUser->name == 'inv_control';
    }

    public function isNormal() {
        return $this->tUser->name == 'nonadmin';
    }

}
