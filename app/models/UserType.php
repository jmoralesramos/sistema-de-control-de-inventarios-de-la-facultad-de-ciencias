<?php

class UserType extends Eloquent {
	protected $fillable = [];
	public $timestamps = false;

	public function users() {
		$this->hasMany('User', 't_user');
	}

}