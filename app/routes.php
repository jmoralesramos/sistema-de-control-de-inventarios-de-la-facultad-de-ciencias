<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['before' => 'auth'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'HomeController@getIndex']);
    Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@getLogout']);

    // Routes of the form entityActionHTTTPMethod

    Route::group(['before' => 'admin'], function () {

        Route::get('user/', ['as' => 'userIndex', 'uses' => 'UserController@getIndex']);        
        Route::get('user/inactives', ['as' => 'userInactives', 'uses' => 'UserController@getInactives']);
        Route::get('user/inactives/sendmail', ['as' => 'userSendMail', 'uses' => 'UserController@getNotification']);
        Route::post('user/inactives/mailsent',['as'=>'userAfterMailSent','uses'=> 'UserController@postNotification']);
        Route::get('user/inactives/sendmasivemail', ['as' => 'userMasiveMail', 'uses' => 'UserController@getMasiveMail']);
        Route::post('user/inactives/masivemailsent',['as'=>'userMasiveMailSent','uses'=> 'UserController@postMasiveMail']);

        Route::post('user/create', ['as' => 'userCreatePost', 'uses' => 'UserController@postCreate']);
        Route::get('user/create', ['as' => 'userCreateGet', 'uses' => 'UserController@getCreate']);

        Route::post('user/delete/{id}', ['as' => 'userDeletePost', 'uses' => 'UserController@postDelete']);

        Route::post('device/delete/{id}', ['as' => 'deviceDeletePost', 'uses' => 'DeviceController@postDelete']);
    });

    Route::get('device/', ['as' => 'deviceIndex', 'uses' => 'DeviceController@getIndex']);
    Route::get('device/partials/{id?}', ['as' => 'devicePartialsIndex', 'uses' => 'DeviceController@getPartials']);

    Route::group(['before' => 'inv_control'], function () {
        Route::post('device/create', ['as' => 'deviceCreatePost', 'uses' => 'DeviceController@postCreate']);
        Route::get('device/create', ['as' => 'deviceCreateGet', 'uses' => 'DeviceController@getCreate']);
    });

    Route::post('user/update/{id}', ['as' => 'userUpdatePost', 'uses' => 'UserController@postUpdate']);
    Route::get('user/update/{id}', ['as' => 'userUpdateGet', 'uses' => 'UserController@getUpdate']);

    Route::post('device/update/{id}', ['as' => 'deviceUpdatePost', 'uses' => 'DeviceController@postUpdate']);
    Route::get('device/update/{id}', ['as' => 'deviceUpdateGet', 'uses' => 'DeviceController@getUpdate']);

    Route::get('/device/{field}/{value}',['as' => 'deviceSearch', 'uses' => 'DeviceController@getSearch']);

    Route::get('/user/devices/ver/{dato}',['DevicesController@verDevice']);    
    Route::get('reports/partials', ['as' => 'reportsPartials', 'uses' => 'ReportsController@getPartials']);         
    Route::get('reports/eprogram', ['as' => 'reportsEducativeProgram', 'uses' => 'ReportsController@getEP']);
    Route::get('reports/eprogram/excel', ['as' => 'reportsGeneralExcel', 'uses' => 'ReportsController@generateGeneralExcel']);             
    Route::get('reports/eprogram/excel/carreer/{carreer}', ['as' => 'reportsCarreerExcel', 'uses' => 'ReportsController@generateCarreerExcel']);       
    
    Route::get('reports/user-carreer/{carreer}', ['as' => 'reportUserCarreer', 'uses' => 'PDFController@generateCarreerPDF']);
    Route::get('reports/devices/user/{id}', ['as' => 'userDevicesReport', 'uses' => 'PDFController@generateDevicesPDF']);
    Route::get('reports/partials/excel/', ['as' => 'reportsPartialsExcel', 'uses' => 'ReportsController@generatePartialsExcel']);  
    Route::get('reports/devices/user/excel/{id}', ['as' => 'userDevicesExcel', 'uses' => 'ReportsController@generateDevicesExcel']);
    
    Route::get('/users/search/','UsersController@searchRequest');
    Route::get('/users/search/{dato}','UsersController@searchResponse');
    Route::get('/users/devices/{dato}','UsersController@myDevices');
    Route::get('/users/devices/ver/{dato}/log','DevicesController@verLogs');
    
});
// route to show the login form
Route::get('login', ['as' => 'login', 'uses' => 'HomeController@showLogin']);
// route to process the form
Route::post('login', ['uses' => 'HomeController@doLogin']);

Route::get('password/remind', ['as' => 'userRemindGet', 'uses' => 'RemindersController@getRemind']);
Route::post('password/remind', ['as' => 'userRemindPost', 'uses' => 'RemindersController@postRemind']);

Route::get('password/reset', ['as' => 'userResetGet', 'uses' => 'RemindersController@getReset']);
Route::post('password/reset', ['as' => 'userResetPost', 'uses' => 'RemindersController@postReset']);

