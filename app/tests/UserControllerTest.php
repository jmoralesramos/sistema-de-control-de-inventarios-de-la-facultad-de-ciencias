<?php

class UserControllerTest extends TestCase {

    function testGetCreate() {
        $respose = $this->call('GET', 'user/create');
        $this->assertResponseOk();
    }

    function testPostCreate() {
        $params = [
            'id_carreer' => 2,
            'name' => 'Test',
            's_father' => 'Tesrcia',
            's_mother' => 'Testca',
            'no_employee' => '666666',
            'email' => 'lucifer@hell.com',
            't_user' => 'admin',
            'password' => Hash::make('secret'),
            'status' => mt_rand(0, 1),
            'active' => mt_rand(0, 1)
        ];

        $response = $this->call('POST', 'user/create', $params);
        $this->assertRedirectedToAction('UserController@getCreate');
        User::all()->last()->delete();
    }

    function testPostCreateFail() {
        $params = [
            'id_carreer' => 2,
            'name' => 'Test',
            's_father' => 'Tesrcia',
            's_mother' => 'Testca',
            'no_employee' => User::all()->first()->no_employee,
            'email' => 'lucifer@hell.com',
            't_user' => 'admin',
            'password' => Hash::make('secret'),
            'status' => mt_rand(0, 1),
            'active' => mt_rand(0, 1)
        ];

        $response = $this->call('POST', 'user/create', $params);
        $this->assertSessionHasErrors();
        $this->assertSessionHasErrors(['no_employee']);
    }

    function testPostUpdate() {
        $params = [
            'id_carreer' => 2,
            'name' => 'Test',
            's_father' => 'Tesrcia',
            's_mother' => 'Testca',
            'no_employee' => '666666',
            'email' => 'lucifer@hell.com',
            // 'password' => Hash::make('secret'),
        ];

        $response = $this->call('POST', 'user/update/' . $id, $params);
        $this->assertRedirectedToAction('UserController@getUpdate');
    }

    function testPostUpdateFail() {
        $params = [
            'id_carreer' => 2,
            'name' => 'Test',
            's_father' => 'Tesrcia',
            's_mother' => 'Testca',
            'no_employee' => User::all()->first()->no_employee,
            'email' => 'lucifer@hell.com',
            //'password' => Hash::make('secret'),
        ];

        $response = $this->call('POST', 'user/update/' . $id, $params);
        $this->assertSessionHasErrors();
        $this->assertSessionHasErrors(['no_employee']);
    }

}
?>