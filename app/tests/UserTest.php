<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTest extends TestCase {
    function testUserCreation() {
        $faker = Faker::create('es_ES');
        $userTypes = ['admin', 'nonadmin'];
        $max = Carreer::all()->count();

        $params = [
            'id_carreer' => $faker->numberBetween(1, $max),
            'name' => $faker->name,
            's_father' => $faker->lastName,
            's_mother' => $faker->lastName,
            'no_employee' => 666666,
            'email' => 'imsoawesome@gmail.com',
            't_user' => $faker->randomElement($userTypes),
            'password' => Hash::make('secret'),
            'status' => mt_rand(0, 1),
            'active' => mt_rand(0, 1)
        ];

        $user = User::create($params);
        $this->assertTrue($user->exists);
        return $user->id;
    }

    function testUserCreationFail() {
        $faker = Faker::create('es_ES');
        $userTypes = ['admin', 'nonadmin'];
        $max = Carreer::all()->count();

        $params = [
            'id_carreer' => $faker->numberBetween(1, $max),
            'name' => $faker->name,
            's_father' => $faker->lastName,
            's_mother' => $faker->lastName,
            'no_employee' => 666666,
            'email' => 'imsoawesome@gmail.com',
            't_user' => $faker->randomElement($userTypes),
            'password' => Hash::make('secret'),
            'status' => mt_rand(0, 1),
            'active' => mt_rand(0, 1)
        ];

        $user = User::create($params);
        $this->assertFalse($user->exists);
    }

    /**
     * @depends testUserCreation
     */
    function testUserUpdate($id) {
        $faker = Faker::create('es_ES');
        $userTypes = ['admin', 'nonadmin'];
        $max = Carreer::all()->count();

        $params = [
            'name' => $faker->name,
            's_father' => $faker->lastName,
            's_mother' => $faker->lastName,
            't_user' => $faker->randomElement($userTypes),
            'password' => Hash::make('secret'),
            'status' => mt_rand(0, 1),
            'active' => mt_rand(0, 1)
        ];

        $user = User::find($id);
        $user->fill($params);
        // $isSaved = $user->save([
        //     'email' => 'required|email|unique:users,email,' . $user->id,
        //     'no_employee' => 'required|numeric|unique:users,email,' . $user->id
        // ]);
        $isSaved = $user->updateUniques();
        $this->assertTrue($isSaved);
        return $user->id;
    }

    /**
     * @depends testUserUpdate
     */
    function testUserDelete($id) {
        $user = User::find($id);
        $this->assertTrue($user->delete());
    }

}
?>