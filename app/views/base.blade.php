<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>SCIC - @yield('title', 'Home')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/js/libs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="/css/side-bar.css" rel="stylesheet">
        <link href="/css/index.css" rel="stylesheet">
        <link href="/css/common.css" rel="stylesheet">
         <link rel="shortcut icon" href="{{ URL::asset('css/images/logo-uabc.png')}}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('css/images/logo-uabc.png')}}" type="image/x-icon">
        <script type="text/javascript" src="{{ URL::asset('js/common.js')}}"></script>
    </head>
    <body>       
        <div id="header" >
                <!-- Brand and toggle get grouped for better mobile display -->
            <div id="header-brand">Sistema de Control de Inventarios de la Facultad de Ciencias</div>
            <img id="logo" src="{{ URL::asset('css/images/image04.png')}}" >
            <nav>                    
                            
                <!-- Collect the nav links, forms, and other content for toggling -->
                <ul>
                    <li><a class="navbar-brand" href="{{ URL::route('index') }}">SCIC</a></li>
                    @if (Auth::check())
                        <li><a href="{{ URL::route('userUpdateGet', ['id' => Auth::user()->id]) }}">Perfil</a></li>
                        <li>
                            <a>Equipos</a>
                            <ul>
                                <li><a href="{{ URL::route('deviceIndex') }}">Todos</a></li>
                                @if(Auth::user()->isNormal())
                                    <li><a href="{{ URL::route('devicePartialsIndex', ['id' => Auth::user()->id]) }}">Parciales</a></li>
                                @else
                                    <li><a href="{{ URL::route('devicePartialsIndex')}}">Parciales</a></li>
                                @endif
                            </ul>
                        </li>
                            @if (Auth::user()->isAdmin())
                            <li><a href="{{ URL::route('userIndex') }}">Profesores</a>
                                <ul>
                                    <li>
                                        <a href="{{ URL::route('userInactives') }}">Inactivos</a>
                                        <a href="{{ URL::route('userMasiveMail') }}">Correo Masivo</a>
                                    </li>
                                </ul>
                            </li>
                                <li><a>Reportes</a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::route('reportsPartials') }}">Parciales</a>
                                            <a href="/users/search/">Reporte por docente</a>
                                            <a href="{{ URL::route('reportsEducativeProgram') }}">Programa Educativo</a>
                                        </li>
                                    </ul>
                                </li>
                          @elseif(Auth::user()->isInvControl())
                            <li><a>Reportes</a>
                                <ul>
                                    <li>
                                        <a href="{{ URL::route('reportsPartials') }}">Parciales</a>
                                        <a href="/users/search/">Reporte por docente</a>
                                        <a href="{{ URL::route('reportsEducativeProgram') }}">Programa Educativo</a>
                                    </li>
                                </ul>
                            </li>                           
                            @endif
                        <li><a href="{{ URL::route('logout') }}">Cerrar Sesion</a></li>
                        <span ><strong>
                            <a href="{{ URL::route('userUpdateGet', ['id' => Auth::user()->id]) }}">{{ Auth::user()->name }}</a> <br>
                             @if (Auth::user()->isAdmin())
                                &lt; Administrador(a) &gt;
                             @elseif(Auth::user()->isInvControl())
                                &lt; Control de Inventario &gt;
                             @elseif(Auth::user()->isNormal())
                                &lt; Profesor(a)/Investigador(a) &gt;
                             @endif
                            </strong></span>    
                       
                    @endif                 
                </ul>                
            </nav>
        </div>             
        <div id="wrapper">     
            <div class="container-fluid" id="main-container">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('status'))
                    <div class="alert alert-info">{{ Session::get('status') }}</div>
                @endif
                @if (Session::has('errors'))
                    <div class="alert alert-danger">{{ HTML::ul($errors->all()) }}</div>
                @endif
                @yield('content')
            </div>
        </div>
        <script data-main="{{ URL::asset('js/main')}}" src="{{ URL::asset('js/libs/requirejs/require.js')}}" ></script>        
              
    </body>
</html>
