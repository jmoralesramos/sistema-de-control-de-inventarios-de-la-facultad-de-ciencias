@extends('base')
@section('title')
Create Device
@stop
@section('content')
@if(!$errors->isEmpty())
<div class="alert alert-danger">
    {{ HTML::ul($errors->all()) }}
</div>
@endif
<span class="row">
<div class="col-md-3">
    <div class="text-info">Campos necesarios marcados con <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span></div>
    {{ Form::open(['route' => 'deviceCreatePost', 'files' => true]) }}
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('id_user', 'Usuario') }}
        {{ Form::select('id_user', $users, NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        @if(Auth::user()->isInvControl())
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        @endif
        {{ Form::label('id_status', 'Estatus') }}
        {{ Form::select('id_status', $statuses, NULL, ['class' => 'form-control', 'disabled' => 'disabled']) }}
    </div>
    <div class="input-group">
        {{ Form::label('no_control', 'No. Control') }}
        {{ Form::text('no_control', NULL, ['class' => 'form-control', 'disabled' => 'disabled']) }}
    </div>

    <div class="input-group">
        {{ Form::label('device_file', 'Archivo') }}
        {{ Form::file('device_file', ['class' => 'form-control']) }}
    </div>

    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('description', 'Descripción') }}
        {{ Form::textarea('description', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('buy_order', 'Orden de Compra') }}
        {{ Form::text('buy_order', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('brand', 'Marca') }}
        {{ Form::text('brand', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('no_serial', 'No. Serie') }}
        {{ Form::text('no_serial', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('no_programa', 'No. Programa') }}
        {{ Form::text('no_programa', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        {{ Form::label('location', 'Ubicación') }}
        {{ Form::textarea('location', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        {{ Form::label('observations', 'Observaciones') }}
        {{ Form::textarea('observations', NULL,['class' => 'form-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('d_entry', 'Fecha de Entrada') }}
        {{ Form::input('date', 'd_entry', NULL, ['class' => 'form-control date-control']) }}
    </div>
    <div class="input-group">
        <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
        {{ Form::label('provider', 'Proveedor') }}
        {{ Form::text('provider', NULL,['class' => 'form-control']) }}
    </div>
    <span class="row">
        <button class="btn btn-default" type="submit">Guardar</button>
        <a class="btn btn-danger pull-right" href=" {{ URL::route('deviceIndex') }} ">Cancel</a>
    </span>
    {{ Form::close() }}
</div>
</span>
@stop