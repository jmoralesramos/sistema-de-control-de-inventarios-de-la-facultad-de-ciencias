@extends('base')
@section('title')
Devices
@stop
@section('content')
<div class="input-group col-md-4">
    <select class="form-control" id="search-select">
        <option value="no_control">Número de Control</option>
        <option value="no_serial">Número de Serie</option>
        <option value="buy_order">Orden de Compra</option>
        <option value="no_programa">Número de Programa</option>
    </select>
    <input class="form-control" id="search-text" type="text">
    <button id="search-button" class="btn btn-default buscar" >Buscar</button><br><br>
    <button id="excel-btn" class="btn btn-success">EXCEL</button>
</div>

@if (Auth::user()->isInvControl())
<div class="row">
    <div class="col-md-1">
        <a href="{{ URL::route('deviceCreateGet') }}" class="btn btn-success">Create</a>
    </div>
</div>
@endif
<span class="row">
<div class="table-responsive">
    {{ Form::open(['route' => 'deviceDeletePost'])}}
    <table class="table table-striped">
        <thead>
            <tr>
                <th>No. Control</th>
                @if (!Auth::user()->isNormal())
                <th>Responsable</th>
                @endif
                <th>Status</th>
                <th>Marca</th>
                <th>No. Serie</th>
                <th>No. Programa</th>
                <th>Ubicaci&oacute;n</th>
                @if(Auth::user()->isAdmin())
                <th class="col-sm-1">Borrar</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($devices as $device)
            <tr {{ $device->status->isLent() ? 'class="warning"' : ''}}>
                <td>
                    <a href="{{ URL::route('deviceUpdateGet', $device->id) }}" class="btn_edit">{{ !is_null($device->no_control) ? $device->no_control : 'Registro Parcial' }}</a>
                </td>
                @if (!Auth::user()->isNormal())
                <td>
                    {{{ !is_null($device->user) ? $device->user->name : 'No Asignado' }}}
                </td>
                @endif
                <td>
                    {{{ $device->status->description }}}
                </td>
                <td>
                    {{{ $device->brand }}}
                </td>
                <td>
                    {{{ $device->no_serial }}}
                </td>
                <td>
                    {{{ $device->no_programa }}}
                </td>
                <td>
                    {{{ $device->location }}}
                </td>
                @if (Auth::user()->isAdmin())
                <td class="col-sm-1">
                    <a class="btn btn-danger device-destroy-button" data-device="{{$device->id}}">Destroy</a>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ Form::close() }}
</div>
</span>
<span class="row">
{{ $devices->links() }}
</span>
@stop