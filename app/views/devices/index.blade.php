@extends('base')
@section('content')
	<h1>Busqueda de dipositivo</h1>
  <input  class="form"  id="abuscar" type="text">
  <select class="form"  id="myselect">
    <option value="nControl">Número de Control</option>
    <option value="nSerie">Número de Serie</option>
    <option value="ordenCompra">Orden de Compra</option>
    <option value="nPrograma">Número de Programa</option>
  </select>
  <button  onclick="Buscar()" class="btn btn-default buscar" >Buscar</button>
  {{ HTML::script('js/busqueda.js') }}
@stop
