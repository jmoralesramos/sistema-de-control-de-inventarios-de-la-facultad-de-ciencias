@extends('base')
@section('content')
<h1>Reporte por docente</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información del Equipo</h4>
    </div>
    @include('devices.userDevices', ['user' => $user])
    <div class="panel-body">
        <a href="#" onclick="window.history.back();return false;" class="btn btn-default">Regresar</a>
        <a class="btn btn-primary" href="{{ URL::route('userDevicesReport', ['id' => $user->id]) }}">PDF</a>
        <a class="btn btn-success" href="{{ URL::route('userDevicesExcel', ['id' => $user->id]) }}">EXCEL</a>
    </div>
</div>
@stop