@extends('base')
@section('content')
	<h1>Busqueda de dipositivo</h1>
  <input  class="form"  id="abuscar" type="text">
  <select class="form"  id="myselect">
    <option value="nControl">Número de Control</option>
    <option value="nSerie">Número de Serie</option>
    <option value="ordenCompra">Orden de Compra</option>
    <option value="nPrograma">Número de Programa</option>
  </select>
  <button  onclick="Buscar()" class="btn btn-default buscar" >Buscar</button>
  {{ HTML::script('js/busqueda.js') }}



	<div class="panel panel-success">
    <div class="panel-heading">
      <h4>Información del Equipo</h4>
    </div>

    <div class="panel-body">
      @if (!empty($device))
   
        <label class="col-sm-2  "><strong>Id:</strong></label>
        <label class="col-sm-2 ">{{ $device->id }} </label><br></br>
        <label class="col-sm-2 ">Usuario:</label>
        <label class="col-sm-2">{{ $device->id_user }} </label><br></br>
        <label class="col-sm-2 ">Estado:</label>
        <label class="col-sm-2 ">{{ $device->id_status }} </label><br></br>
        <label class="col-sm-2 ">No. Control:</label>
        <label class="col-sm-2 ">{{ $device->no_control }} </label><br></br>
        <label class="col-sm-2 ">Descripcion:</label>
        <label class="col-sm-2 ">{{ $device->description }} </label><br></br><br></br><br></br>
        <label class="col-sm-2 "> Orden de compra: </label>
        <label class="col-sm-2 ">{{ $device->buy_order }} </label><br></br>
        <label class="col-sm-2 ">Marca:</label>
        <label class="col-sm-2 ">{{ $device->brand }} </label><br></br>
        <label class="col-sm-2 "> No. Serie:</label>
        <label class="col-sm-2 ">{{ $device->no_serial }} </label><br></br>
        <label class="col-sm-2 ">No. Programa:</label>
        <label class="col-sm-2 ">{{ $device->no_programa }} </label><br></br>
        <label class="col-sm-2 ">Localización:</label>
        <label class="col-sm-2 ">{{ $device->location }} </label><br></br>
        <label class="col-sm-2 ">Observations: </label>
        <label class="col-sm-2 ">{{ $device->observations }} </label><br></br><br></br><br></br>
        <label class="col-sm-2 ">Fecha: </label>
        <label class="col-sm-2 ">{{ $device->d_entry }} </label><br></br>
        <label class="col-sm-2 ">Provedor:</label>
        <label class="col-sm-2 ">{{ $device->provider }} </label><br></br
      @else
        <p>
          No existe información para éste dispositivo.
        </p>
      @endif
      <a href="#" onclick="window.history.back();return false;"  class="btn btn-default">Regresar</a>
		</div>
	</div>
@stop
