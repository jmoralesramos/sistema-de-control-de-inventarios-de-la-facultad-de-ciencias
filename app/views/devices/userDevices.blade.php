<table class="table">
    <thead>
        <tr>
            <th>No. Control</th>
            <th>Descripci&oacute;n</th>
            <th>Orden de compra</th>
            <th>Marca</th>
            <th> Serie</th>
            <th>No. Programa</th>
            <th> Estado</th>
            <th>Ubicaci&oacute;n</th>
            <th>Observaciones</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($user->devices as $device)
        <tr>
            <td>
                <a href="{{ URL::route('deviceUpdateGet', $device->id) }}" class="btn_edit">{{ !is_null($device->no_control) ? $device->no_control : 'Registro Parcial' }}</a>
            </td>
            <td>{{ $device->description }}</td>
            <td>{{ $device->buy_order }}</td>
            <td>{{ $device->brand }}</td>
            <td>{{ $device->no_serial }}</td>
            <td>{{ $device->no_programa }}</td>
            <td>{{ $device->status->description }}</td>
            <td>{{ $device->location }}</td>
            <td>{{ $device->observations }}</td>
        </tr>
        @endforeach
    </tbody>
</table>