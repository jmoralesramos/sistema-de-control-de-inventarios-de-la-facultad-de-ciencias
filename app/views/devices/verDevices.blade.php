@extends('base')
@section('content')

	<div class="panel panel-success">
    <div class="panel-heading">
      <h4>Información del Equipo</h4>
    </div>

    <div class="panel-body">
      @if (!empty($device))
   
        <label class="col-sm-2  "><strong>Id:</strong></label>
        <label class="col-sm-2 ">{{ $device->id }} </label><br></br>
        <label class="col-sm-2 ">Usuario:</label>
        <label class="col-sm-2">{{ $device->id_user }} </label><br></br>
        <label class="col-sm-2 ">Estado:</label>
        <label class="col-sm-2 ">{{ $device->id_status }} </label><br></br>
        <label class="col-sm-2 ">No. Control:</label>
        <label class="col-sm-2 ">{{ $device->no_control }} </label><br></br>
        <label class="col-sm-2 ">Descripci&oacute;n:</label>
        <label class="col-sm-2 ">{{ $device->description }} </label><br></br><br></br><br></br>
        <label class="col-sm-2 "> Orden de compra: </label>
        <label class="col-sm-2 ">{{ $device->buy_order }} </label><br></br>
        <label class="col-sm-2 ">Marca:</label>
        <label class="col-sm-2 ">{{ $device->brand }} </label><br></br>
        <label class="col-sm-2 "> No. Serie:</label>
        <label class="col-sm-2 ">{{ $device->no_serial }} </label><br></br>
        <label class="col-sm-2 ">No. Programa:</label>
        <label class="col-sm-2 ">{{ $device->no_programa }} </label><br></br>
        <label class="col-sm-2 ">Ubicaci&oacute;n:</label>
        <label class="col-sm-2 ">{{ $device->location }} </label><br></br>
        <label class="col-sm-2 ">Observaciones: </label>
        <label class="col-sm-2 ">{{ $device->observations }} </label><br></br><br></br><br></br>
        <label class="col-sm-2 ">Fecha: </label>
        <label class="col-sm-2 ">{{ $device->d_entry }} </label><br></br>
        <label class="col-sm-2 ">Proveedor:</label>
        <label class="col-sm-2 ">{{ $device->provider }} </label><br></br>
					     
      @else
        <p>
          No existe información para éste dispositivo.
        </p>
      @endif
      <a href="#" onclick="window.history.back();return false;" class="btn btn-default">Regresar</a>
		</div>
	</div>
@stop
