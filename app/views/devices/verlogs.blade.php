@extends('base')
@section('content')

	<h1>Logs del equipo</h1>



	<div class="panel panel-success">
    <div class="panel-heading">
      <h4>Descripción: {{ $description }}</h4>
    </div>
      @if (!empty($logs))
        		<table class="table">
				<thead>
					<tr>
            <th>Fecha de registro</th>
						<th>Responsable anterior</th>
            <th>Estatus</th>
            
					</tr>
				</thead>
				<tbody>
					@foreach($logs as $log)
						<tr>
              <td>{{ $log["date"]  }}</td>
              <td>{{ $log["user"] }} </td>
              <td>{{ $log["status"] }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>

    <div class="panel-body">
      <a href="#" onclick="window.history.back();return false;" class="btn btn-default">Regresar</a>
		</div>
       @else
        <p>
          No existe logs para éste dispositivo.
        </p>
        <div class="panel-body">
          <a href="#" onclick="window.history.back();return false;" class="btn btn-default">Regresar</a>
        </div>
      @endif
	</div>

@stop
