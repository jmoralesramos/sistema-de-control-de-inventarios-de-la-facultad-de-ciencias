@extends('errors.base')
@section('title')
Forbidden
@stop
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">Tsk Tsk</div>
    <div class="panel-body">
        You don't have permissions to access this url.
    </div>
</div>
@stop