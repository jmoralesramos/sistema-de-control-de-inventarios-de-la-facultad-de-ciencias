@extends('errors.base')
@section('title')
Not Found
@stop
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">Object not found</div>
    <div class="panel-body">
        The object you're looking for doesn't exist.
    </div>
</div>
@stop