<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>SCIC - @yield('title', 'Home')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/js/libs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="/css/side-bar.css" rel="stylesheet">
        <link href="/css/index.css" rel="stylesheet">
        <link href="/css/common.css" rel="stylesheet">
         <link rel="shortcut icon" href="{{ URL::asset('css/images/logo-uabc.png')}}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('css/images/logo-uabc.png')}}" type="image/x-icon">
        <script type="text/javascript" src="{{ URL::asset('js/common.js')}}"></script>
    </head>
    <body>       
        <div id="wrapper">     
            <div class="container-fluid" id="main-container">
                @yield('content')
                <a href="{{ URL::previous() }}" class="btn btn-danger">Take Me Back</a>
            </div>
        </div>
    </body>
</html>
