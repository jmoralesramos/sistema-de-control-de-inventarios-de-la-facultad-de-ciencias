
@extends('base')

@section('title')
Login
@stop

@section('content')
<div class="row">
    <center>
        <div id="login-div" style="text-align: left">
            
        {{ Form::open(array('url' => 'login', 'class' => '')) }}
            <h1>Login</h1>
        
            <!-- if there are login errors, show them here -->
            @if(!$errors->isEmpty())
            <div class="alert alert-danger">
                {{ $errors->first('email') }}
                {{ $errors->first('password') }}
            </div>
            @endif
    
            <div class="form-group">
                {{ Form::label('email', 'Email Address') }}
                {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Password') }}
                {{ Form::password('password', ['class' => 'form-control']) }}
            </div>
    
            {{ Form::submit('Login', ['class' => 'btn btn-primary','style'=>'width:140px']) }}
            <a href="{{ URL::route('userRemindGet') }}" class="btn btn-default">Forgot Password</a>
        {{ Form::close() }}
        </div>
    </center>
</div>
@stop