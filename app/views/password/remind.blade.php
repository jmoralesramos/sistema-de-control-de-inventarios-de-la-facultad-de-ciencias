@extends('base')

@section('title')
Password Remind
@stop

@section('content')
<div class="row">
    <div class="col-md-4 col-offset-xs-1">
        <h1>Password Remind</h1>
        {{ Form::open(['url' => URL::route('userRemindPost')])}}
            {{ Form::label('email') }}
            {{ Form::text('email', NULL, ['class' => 'form-control']) }}
            {{ Form::submit('Remind', ['class' => 'btn btn-default'])}}
        {{ Form::close() }}
    </div>
</div>
@stop