@extends('base')
@section('content')
<link rel="stylesheet" href="<?php echo asset('css/reports/eprogram.css')?>" type="text/css">
<script type="text/javascript" src="{{URL::asset('js/libs/jquery/dist/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/libs/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/reports/eprogram.js')}}"></script>

    <h2>Reporte General de Equipos por Programa Educativo</h2>
    <div class="form-inline">
        <div class="form-group" id="filtroCarreras">
            <div class="col-md-6">
                <button id="pdf-btn" class="btn btn-primary">PDF</button>
                <button id="excel-btn" class="btn btn-success">EXCEL</button>           
            </div>
            <div class="col-md-4">
                <select class="form-control" id="carreras" onchange="javascript: filtrarDatos();">
                    <option value="0">Filtrar por carrera</option>
                     <option value="admin">Admins</option>
                     @for($i = 0; $i < count($data[0]); $i++)
                     <option value="{{$data[0][$i]->id}}">{{$data[0][$i]->name}}</option>
                     @endfor
                </select>
            </div>
    </div>
    </div>
    @if(count($data[1]) > 0)
    <div id="reports">
    @for($i = 0; $i < count($data[1]); $i++)
        @if(count($data[1][$i]->devices) >0)
        @include('reports.tableView', ['user' => $data[1][$i]])
        @endif
    @endfor
    </div>
    @else
        <h2>No existen registros parciales de equipos</h2>
    @endif

@stop


