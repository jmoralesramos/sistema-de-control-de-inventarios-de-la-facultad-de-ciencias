@extends('base')
@section('content')

<link rel="stylesheet" href="/css/reports/partials.css" type="text/css"> 
<script type="text/javascript" src="{{URL::asset('js/libs/jquery/dist/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/libs/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/reports/partials.js')}}"></script>
<h2 >Reporte de registros parciales</h2>


    @if(count($devices) > 0)
    <button id="excel-btn" class="btn btn-success" style="margin-bottom: 10px">EXCEL</button>
        <table id="tPartialReports">
            <tr>
                <th><a onclick="javascript: sortTable('tPartialReports',0,'D','ymd');">Fecha</a></th>
                <th><a onclick="javascript: sortTable('tPartialReports',1,'T');">Nombre de Maestro</a></th>
                <th>Descripci&oacute;n</th>
                <th><a onclick="javascript: sortTable('tPartialReports',3,'T');">Proveedor</a></th>
                <th><a onclick="javascript: sortTable('tPartialReports',4,'N');">No. Programa</a></th>
                <th><a onclick="javascript: sortTable('tPartialReports',5,'N');">No. Serie</a></th>
                <th><a onclick="javascript: sortTable('tPartialReports',6,'T');">Carrera</a></th>
            </tr>
        @for( $i = 0; $i < count($devices); $i ++)
            @if( $i % 2)
            <tr class="oddRow">
                <td ><span class="popoverText" data-content=" {{$devices[$i]->d_entry}} "> {{$devices[$i]->d_entry}}</span></td>
                <td ><span class="popoverText" data-content=" {{$devices[$i]->t_name}} "> {{$devices[$i]->t_name}}</span></td>
                <td> <span class="popoverText" data-content=" {{$devices[$i]->description}}">{{$devices[$i]->description}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->provider}}">{{$devices[$i]->provider}}</span></td>
                <td><span class="popoverText" data-content="{{$devices[$i]->no_programa}}"> {{$devices[$i]->no_programa}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->no_serial}}">{{$devices[$i]->no_serial}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->c_name}}">{{$devices[$i]->c_name}}</span></td>
            </tr>
            @else
            <tr class="evenRow">
                <td ><span class="popoverText" data-content="{{$devices[$i]->d_entry}}"> {{$devices[$i]->d_entry}}</span></td>
                <td ><span class="popoverText" data-content=" {{$devices[$i]->t_name}} "> {{$devices[$i]->t_name}}</span></td>
                <td> <span class="popoverText" data-content=" {{$devices[$i]->description}}">{{$devices[$i]->description}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->provider}}">{{$devices[$i]->provider}}</span></td>
                <td><span class="popoverText" data-content="{{$devices[$i]->no_programa}}"> {{$devices[$i]->no_programa}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->no_serial}}">{{$devices[$i]->no_serial}}</span></td>
                <td> <span class="popoverText" data-content="{{$devices[$i]->c_name}}">{{$devices[$i]->c_name}}</span></td>
            </tr>
            @endif
        @endfor
        </table>
    @else
        <h2>No existen registros parciales de equipos</h2>
    @endif
    

@stop


