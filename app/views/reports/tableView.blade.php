@if($user->t_user == 1)
<div class="report-table carreer_admin"> 
@else
<div class="report-table carreer_{{ $user->id_carreer}}">
@endif

    <p style="padding-top: 10px">{{$user->name." ".$user->s_father." ".$user->s_mother}}</p>
    <table id="table{{$user->id}}">
        <tr>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',0,'D','ymd');">Fecha</a></th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',1,'T');">No. Control</a></th>
            <th>Descripci&oacute;n</th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',2,'T');">Orden de Compra</a></th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',3,'T');">Marca</a></th>            
            <th><a onclick="javascript: sortTable('table{{$user->id}}',4,'T');">No. Serie</a></th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',5,'N');">No. Programa</a></th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',6,'T');">Estado</a></th>
            <th><a onclick="javascript: sortTable('table{{$user->id}}',7,'T');">Ubicaci&oacute;n</a></th>
            <th>Observaciones</th>
            
        </tr>

        @if(!is_null($user->devices))
            @for( $j = 0; $j < count($user->devices); $j ++)
                @if( $j % 2)
                <tr class="oddRow">
                    <td ><span class="popoverText" data-content=" {{$user->devices[$j]->d_entry}} "> {{$user->devices[$j]->d_entry}}</span></td>
                    <td> <span class="popoverText" data-content=" {{$user->devices[$j]->no_control}}">{{$user->devices[$j]->no_control}}</span></td>
                    <td><span class="popoverText" data-content="{{$user->devices[$j]->description}}"> {{$user->devices[$j]->description}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->buy_order}}">{{$user->devices[$j]->buy_order}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->brand}}">{{$user->devices[$j]->brand}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->no_serial}}">{{$user->devices[$j]->no_serial}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->no_programa}}">{{$user->devices[$j]->no_programa}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->status->description}}">{{$user->devices[$j]->status->description}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->location}}">{{$user->devices[$j]->location}}</span></td>                                        
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->observations}}">{{$user->devices[$j]->observations}}</span></td>
                </tr>
                @else
                <tr class="evenRow">
                    <td ><span class="popoverText" data-content=" {{$user->devices[$j]->d_entry}} "> {{$user->devices[$j]->d_entry}}</span></td>
                    <td> <span class="popoverText" data-content=" {{$user->devices[$j]->no_control}}">{{$user->devices[$j]->no_control}}</span></td>
                    <td><span class="popoverText" data-content="{{$user->devices[$j]->description}}"> {{$user->devices[$j]->description}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->buy_order}}">{{$user->devices[$j]->buy_order}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->brand}}">{{$user->devices[$j]->brand}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->no_serial}}">{{$user->devices[$j]->no_serial}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->no_programa}}">{{$user->devices[$j]->no_programa}}</span></td>                    
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->status->description}}">{{$user->devices[$j]->status->description}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->location}}">{{$user->devices[$j]->location}}</span></td>
                    <td> <span class="popoverText" data-content="{{$user->devices[$j]->observations}}">{{$user->devices[$j]->observations}}</span></td>
                </tr>
                @endif
            @endfor
        @endif
    </table>
</div>