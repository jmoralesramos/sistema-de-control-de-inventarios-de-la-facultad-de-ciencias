<html>
<head>
</head>
<body>
    <style type="text/css">
        {{ file_get_contents(public_path().'/css/common.css') }}
        {{ file_get_contents(public_path().'/css/reports/eprogram.css') }}
    </style>
    @foreach($users as $user)
        @if(!is_null($user->devices))
            @include('reports.tableView', ['user' => $user])
        @endif
    @endforeach
</body>
</html>