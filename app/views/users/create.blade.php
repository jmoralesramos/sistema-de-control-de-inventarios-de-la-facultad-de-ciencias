@extends('base')
@section('title')
Create User
@stop
@section('content')
@if(!$errors->isEmpty())
    <div class="alert alert-danger">
        {{ HTML::ul($errors->all()) }}
    </div>
@endif
<span class="row">
    {{ Form::open(['route' => 'userCreatePost', 'class' => 'form-inline col-md-6']) }}
    <span class="row">
        <div class="form-group">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('id_carreer', 'Carrera') }}
            {{ Form::select('id_carreer', $carreers, NULL, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('t_user', 'Tipo Usuario:') }}
            {{ Form::select('t_user', $userTypes, NULL, ['class' => 'form-control']) }}
        </div>
    </span>
    <span class="row">
        <div class="form-group">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', NULL, ['class' => 'form-control']) }}
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('s_father', 'Paterno') }}
            {{ Form::text('s_father', NULL, ['class' => 'form-control']) }}
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('s_mother', 'Materno') }}
            {{ Form::text('s_mother', NULL, ['class' => 'form-control']) }}
        </div>
    </span>
    <span class="row">
        <div class="form-group">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('no_employee', 'No. Empleado') }}
            {{ Form::text('no_employee', NULL, ['class' => 'form-control']) }}
        </div>
    </span>
    <span class="row">
        <div class="form-group">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('email', 'Correo') }}
            {{ Form::text('email', NULL, ['class' => 'form-control']) }}
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('password', 'Contraseña') }}
            {{ Form::password('password', ['class' => 'form-control']) }}
        </div>
    </span>
    <span class="row">
        <div class="form-group">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('status', 'Estatus') }}
            {{ Form::checkbox('status', NULL, NULL,['class' => 'form-control']) }}
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            {{ Form::label('active', 'Activo') }}
            {{ Form::checkbox('active', NULL, NULL, ['class' => 'form-control']) }}
        </div>
    </span>
    <span class="row col-md-11">
        {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
        <a class="btn btn-danger pull-right" href=" {{ URL::previous() }} ">Cancelar</a>
    </span>
    {{ Form::close() }}
</span>
@stop