@extends('base')
@section('content')
<div style="text-align: right; font-size: 16px"><a href="{{ URL::route('userSendMail') }}">Enviar recordatorio de revisi&oacute;n de equipo</a></div>
    <h2>Profesores / Investigadores Inactivos</h2>

    @if(count($users) > 0)
    <p><b>Revisi&oacute;n:</b>  <b style="color: green">{{count($tUsers) - count($users) }} CONFIRMADOS</b>, <b style="color: red">{{count($users)}} PENDIENTES</b></p>
        <table id="tInactives">
            <tr>                
                <th><a onclick="javascript: sortTable('tInactives',0,'T');">Nombre de Maestro</a></th>                
                <th><a onclick="javascript: sortTable('tInactives',1,'N');">No. Empleado</a></th>
                <th><a onclick="javascript: sortTable('tInactives',2,'T');">Email</a></th>
                <th><a onclick="javascript: sortTable('tInactives',3,'T');">Tipo de Usuario</a></th>
                <th><a onclick="javascript: sortTable('tInactives',4,'T');">Carrera</a></th>
            </tr>
        @for( $i = 0; $i < count($users); $i ++)
            @if( $i % 2)
            <tr class="oddRow">
                <td > {{$users[$i]->name}} {{$users[$i]->ap}} {{$users[$i]->am}}</td>
                <td > {{$users[$i]->no_employee}}</td>
                <td> {{$users[$i]->email}}</td>
                @if($users[$i]->tUser->id == 1)
                    <td>Aministrador</td>
                @elseif($users[$i]->tUser->id == 2)
                    <td>Profesor/Investigador</td>
                @elseif($users[$i]->tUser->id == 3)
                    <td>Control de Inventarios</td>
                @endif
                <td>{{$users[$i]->carreer->name}}</td>
            </tr>
            @else
            <tr class="evenRow">
                 <td > {{$users[$i]->name}} {{$users[$i]->ap}} {{$users[$i]->am}}</td>
                <td > {{$users[$i]->no_employee}}</td>
                <td> {{$users[$i]->email}}</td>
                @if($users[$i]->tUser->id == 1)
                    <td>Aministrador</td>
                @elseif($users[$i]->tUser->id == 2)
                    <td>Profesor/Investigador</td>
                @elseif($users[$i]->tUser->id == 3)
                    <td>Control de Inventarios</td>
                @endif
                <td>{{$users[$i]->carreer->name}}</td>
            </tr>
            @endif
        @endfor
        </table>
    @else
        <h2>No hay profesores/investigadores inactivos</h2>
    @endif
@stop

