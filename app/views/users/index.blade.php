@extends('base')

@section('content')
<a class="btn btn-primary" href="{{ URL::route('userCreateGet') }}">Create</a>
{{ Form::open(['route' => 'deviceDeletePost'])}}
<div class="table-responsive">
<table class="table table-striped">
    <thead>
        <th>Nombre</th>
        <th>No. Empleado</th>
        <th>Carrera</th>
        <th>Estatus</th>
        <th>Tipo de Usuario</th>
        <th>Borrar</th>
    </thead>
    @foreach ($users as $user)
    <tr>
        <td><a href="{{ URL::route('userUpdateGet', $user->id) }}" class="btn_edit">{{{ $user->name . ' ' . $user->s_father . ' ' . $user->s_mother }}}</a> </td>
        <td>{{ $user->no_employee }}</td>
        <td>{{ !is_null($user->id_carreer) ? $user->carreer->name : 'Ninguna' }}</td>
        <td>{{ $user->status ? 'Si' : 'No'}}</td>
        <td>{{ $user->tUser->name }}</td>
        <td>{{ Form::submit('Destroy', ['formaction' => URL::route('userDeletePost', $user->id), 'method' => 'POST', 'class' => 'btn btn-danger']) }}</td>
    </tr>
    @endforeach
</table>
{{ $users->links() }}
</div>
{{ Form::close() }}
@stop