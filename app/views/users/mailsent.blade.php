@extends('base')

@section('content')
<div style="text-align: right; font-size: 16px"><a href="{{ URL::route('userInactives') }}">Volver a lista de profesores/investigadores inactivos</a></div>
<center>
<h1>Los correos han sido enviados</h1>
<h4>Se enviaron un total de <b> {{$totalMails}} </b> correos</h4>
<h4><b>Nota: verifique en la cuenta de correo que todos hayan sido enviados exitosamente.</b></h4>
</center>
@stop