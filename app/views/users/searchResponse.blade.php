  @extends('base')
@section('content')
		<h1>Reporte por docente</h1>
 Nombre:<input  class="form"  id="abuscar" type="text">
  <button  onclick="BuscarDocente()" class="btn btn-default buscar" >Buscar</button>


	<div class="panel panel-success">
    <div class="panel-heading">
      <h4>Información del Equipo</h4>
    </div>

        		<table class="table">
				<thead>
					<tr>
            <th>Carrera</th>
						<th>Numero de empleado</th>
						<th>Nombre</th>
						<th>Ver</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
              <td>{{ $user->isNormal() ? $user->carreer->name : 'Ninguna' }}</td>
							<td>{{ $user->no_employee }}</td>
							<td>{{ $user->name }} {{ $user->s_father }} {{ $user->s_mother }}</td>
							<td>
								<a href="/users/devices/{{ $user->id }}"
                  <span class="label label-info">Ver</span>
                </a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
    <div class="panel-body">
	</div>
  {{ HTML::script('js/busqueda.js') }}
@stop

