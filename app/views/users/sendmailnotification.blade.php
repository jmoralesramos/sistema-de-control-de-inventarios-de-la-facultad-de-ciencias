@extends('base')
@section('content')
    <link href="/css/users/sendM.css" rel="stylesheet">
    <h2>Profesores / Investigadores Inactivos</h2>

    @if(count($users) > 0)   
    {{ Form::open(['route' => 'userAfterMailSent'])}}
    
    <button id="enviar" class="btn btn-success" type="submit">Enviar</button>  
        <div id="mailsTA">
            <textarea name="mails" id="mails" >@for( $i = 0; $i < count($users); $i ++)@if($i == 0){{$users[$i]->email}} @else ,{{$users[$i]->email}} @endif @endfor</textarea>
        </div>
        <div id="subjectdiv">
            <b>Asunto: </b> <input name="subject" id="subject" type="text" value="Recordatorio de revisi&oacute;n de equipo">
        </div>
        <div id="messagediv">
            <textarea id="message" name="message" >Profesor(a):
Le recordamos que el ____ es la fecha l&iacute;mite para realizar la revisi&oacute;n de los equipos que tiene asignado.

Agradeceremos ampliamente que realice su revisi&oacute;n antes de esta fecha, accesando a su cuenta en la siguiente direcci&oacute;n:

http://sici.uabc.mx
            </textarea>
        </div>
    {{ Form::close() }}
    @else
        <h2>No hay profesores/investigadores inactivos</h2>
    @endif
@stop


