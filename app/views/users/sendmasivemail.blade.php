@extends('base')
@section('content')
    <link href="/css/users/sendM.css" rel="stylesheet">
    <h2>Enviar correo masivo</h2>

    @if(count($users) > 0)   
    {{ Form::open(['route' => 'userMasiveMailSent'])}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="margin-top: 300px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Confirmar env&iacute;o de correo</h4>
          </div>
          <div class="modal-body">
              Todos los profesores se asignar&aacute;n como pendientes de revisar su equipo
          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Enviar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>            
          </div>
        </div>
      </div>
    </div>
    <button id="enviar" class="btn btn-success"  data-toggle="modal" data-target="#myModal" type="button">Enviar</button>  
        <div id="mailsTA">
            <textarea name="mails" id="mails" >@for( $i = 0; $i < count($users); $i ++)@if($i == 0){{$users[$i]->email}} @else ,{{$users[$i]->email}} @endif @endfor</textarea>
        </div>
        <div id="subjectdiv">
            <b>Asunto: </b> <input name="subject" id="subject" type="text" value="Solicitud de revisi&oacute;n de equipo">
        </div>
        <div id="messagediv">
            <textarea id="message" name="message" >Profesor(a):
Le informamos que el ____ es la fecha l&iacute;mite para realizar la revisi&oacute;n de los equipos que tiene asignado.

Agradeceremos ampliamente que realice su revisi&oacute;n antes de esta fecha, accesando a su cuenta en la siguiente direcci&oacute;n:

http://sici.uabc.mx
            </textarea>
        </div>
    {{ Form::close() }}
    @else
        <h2>No hay profesores/investigadores activos</h2>
    @endif
@stop


