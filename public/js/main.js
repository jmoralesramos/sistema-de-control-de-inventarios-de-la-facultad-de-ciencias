require.config({
    paths: {
        'jquery': 'libs/jquery/dist/jquery',
        'bootstrap': 'libs/bootstrap/dist/js/bootstrap',
        'bootbox' : 'libs/bootbox/bootbox'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'bootbox' : {
            deps: ['bootstrap']
        }
    }
});

define(['jquery', 'bootstrap', 'restscic','bootbox'], function($, bootstrap, restscic) {
    $(function() {
        $('.device-destroy-button').click(function(){
            var self = $(this);
            bootbox.confirm('¿Quieres eliminar este registro?', function(result){
                if(result) {
                    var row = self.parent().parent(); 
                    restscic.deleteDevice(self.data('device')).success(function() {
                        $('#main-container').prepend('<div class="alert alert-info">Dispositivo borrado con éxito.</div>');
                        row.remove();
                    });
                }
            });
        });

        $('#search-button').click(function(){
            var query = $( "#search-text" ).val();
            if (query.trim() !== "") {
                document.location.href = "/device/" + $( "#search-select" ).val()+ "/"+ query;
            } else {
                bootbox.alert('Dato de búsqueda vacío');
            }
        });

        $('#pdf-btn').click(function(){
            document.location.href = '/reports/user-carreer/' + $('#carreras').val();
        });
        
         $('#excel-btn').click(function(){
            //document.location.href = '/reports/partials/excel/';
            var currentLocation = window.location.href;
            if( currentLocation.search("reports/eprogram") !== -1){
                if($("#carreras").val() === "0")
                    document.location.href = '/reports/eprogram/excel/';                
                else{
                    document.location.href = '/reports/eprogram/excel/carreer/'+ $('#carreras').val();
                }
            }
            else if( currentLocation.search("reports/partials") !== -1){                
                document.location.href = '/reports/partials/excel/';
            }
            else if( currentLocation.search("device/partials") !== -1){                
                document.location.href = '/reports/partials/excel/';
            }
            else if( currentLocation.search("device") !== -1){                
                document.location.href = '/reports/devices/user/excel/0';
            }
        });
    });
});
