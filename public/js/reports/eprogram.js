$(document).ready(function() {
   
    $(".popoverText").popover({
        html: true,
        animation:false,
        trigger: 'manual',
        placement: "bottom",
        template: "<div class='popover'><div class='arrow'></div><div class='popover-content' ></div></div>"
    }).click(function(event){                 
        $('.popoverText').each(function(index,element){            
            if( element !== event.target){
                    $(element).parent().children(':last-child').hide();
            }
            else{                
                $(element).parent().children(':last-child').toggle();
                var left = $(element).parent().offset().left;
                $(element).parent().children(':last-child').offset({left:left});
                
            }
        });        
    });
       
    $('.popoverText').each(function(index,element){            
        $(element).popover('show');
        $(element).parent().children(':last-child').hide();       
    });
    
});

function filtrarDatos(){
    var filtroC = $('#carreras').val();
    
    var select = document.getElementById("carreras").options;
    if(filtroC !== '0'){
        for (var i = 0; i < select.length; i++) {        
           if (select[i].value !== filtroC) {
             $('.carreer_'+select[i].value).hide();
           }
           else
               $('.carreer_'+filtroC).show();
        }
    }
    else{
        for (var i = 1; i < select.length; i++) {                    
               $('.carreer_'+select[i].value).show();
        }
        
    }
}