$(document).ready(function() {
   
    $(".popoverText").popover({
        html: true,
        animation:false,
        trigger: 'manual',
        placement: "bottom",
        template: "<div class='popover'><div class='arrow'></div><div class='popover-content' ></div></div>"
    }).click(function(event){                 
        $('.popoverText').each(function(index,element){            
            if( element !== event.target){
                    $(element).parent().children(':last-child').hide();
            }
            else{                
                $(element).parent().children(':last-child').toggle();
                var left = $(element).parent().offset().left;
                $(element).parent().children(':last-child').offset({left:left});
                
            }
        });        
    });
       
    $('.popoverText').each(function(index,element){            
        $(element).popover('show');
        $(element).parent().children(':last-child').hide();       
    });
    
});