define('restscic', ['jquery'], function($) {

    var ROOT = '/';

    var DEVICE_ROOT = ROOT + 'device/';
    var DEVICE_DELETE = DEVICE_ROOT + 'delete/'

    var deleteDevice = function(id) {
        return $.ajax({
            url: DEVICE_DELETE + id,
            type: 'POST'
        });
    }

    return {
        ROOT : '/',
        deleteDevice: deleteDevice
    }
});