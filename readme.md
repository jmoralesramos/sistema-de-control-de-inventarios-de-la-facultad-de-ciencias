# Sistema de Control de Inventario de la Facultad de Ciencias

This webapp is a tool to manage the inventory of the Facultad de Ciencias.

To use correctly:
It's necessary to have [Bower] installed. Then go to public/ and run ``bower install`` in order to install all dependencies.


[Bower]: http://bower.io/
